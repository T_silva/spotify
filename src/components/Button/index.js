import React from 'react';
import { TouchableOpacity, Text, StyleSheet, StackNavigator } from 'react-native';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';


export default class Button extends React.Component {
    constructor(props) {
        super(props)
    }

    render() {
        return (
            <TouchableOpacity style={[styles.button, { backgroundColor: this.props.color }]} >
                <Text style={styles.text}>{this.props.title}</Text>
            </TouchableOpacity>
        );
    }

}
const styles = StyleSheet.create({
    button: {
        width: wp('70%'),
        height: hp('6%'),
        borderRadius: wp('6%'),
        alignItems: 'center',
        justifyContent: 'center'
    },
    text: {
        color: '#fff',
        fontSize: wp('4%')
    }
})
