import React from 'react';
import { View, Text, Image, StyleSheet } from 'react-native';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';

// import { Container } from './styles';

export default function Card() {
    return (
        <View style={styles.container}>
            <Image style={styles.imageCard} source={{ uri: 'http://encurtador.com.br/vFGT1' }} />
            <Text style={styles.title}>Boyce Avenue Acoustic Covers</Text>
        </View>
    );
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    imageCard: {
        width: wp('28%'),
        aspectRatio: 1
    },
    title: {
        color: '#fff',
        fontWeight: 'bold',
        marginTop: wp('2%')
    }
})
