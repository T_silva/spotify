import React from 'react';
import { View, Text, StyleSheet } from 'react-native';

import Card from '../Card'
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';


export default class CardList extends React.Component {
    constructor(props) {
        super(props)
    }
    render() {
        return (
            <View style={styles.recentlyPlayedContainer}>
                <Text style={styles.recentlyPlayedTitle}>{this.props.title}</Text>
                <View style={styles.recentlyPlayedCardContainer}>
                    <Card />
                    <Card />
                    <Card />
                </View>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    recentlyPlayedContainer: {
        marginTop: wp('10%')
    },
    recentlyPlayedTitle: {
        fontSize: wp('5%'),
        color: '#fff',
        fontWeight: 'bold'
    },
    recentlyPlayedCardContainer: {
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'flex-start',
        marginTop: wp('5%')
    }
})
