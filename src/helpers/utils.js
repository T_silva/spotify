import { Dimensions } from 'react-native'
const deviceSize = () => {
    const windowWidth = Dimensions.get('window').width
    if (windowWidth >= 320 && windowWidth <= 413) {
        return 'small'
    } else if (windowWidth >= 414 && windowWidth <= 767) {
        return 'large'
    }
    return 'medium'
}

export default deviceSize