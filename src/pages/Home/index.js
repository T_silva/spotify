import React from 'react';
import { View, Text, StyleSheet } from 'react-native';

import CardList from '../../components/CardList'
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';

// import { Container } from './styles';

export default function Home() {
    return (
        <View style={styles.container}>
            <CardList title={"Tocado recentemente"} />
            <CardList title={"Não sei dos seus ouvidos"} />
            <CardList title={"Seus 10 artistas mais ouvidos nos últimos 3 anos"} />
            <CardList title={"Seus 10 artistas mais ouvidos nos últimos 3 anos"} />
            <CardList title={"Seus 10 artistas mais ouvidos nos últimos 3 anos"} />
        </View>
    );
}

const styles = StyleSheet.create({
    container: {
        width: wp('100%'),
        backgroundColor: '#000',
        padding: wp('5%'),
    }
})
