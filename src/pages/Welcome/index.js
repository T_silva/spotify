import React from 'react';
import { View, Text, ImageBackground, Image, StyleSheet } from 'react-native';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';

import Button from '../../components/Button'

// import { Container } from './styles';

export default function Welcome() {

    return (
        <View style={styles.container}>
            <ImageBackground source={require('../../assets/welcome-bg.jpg')} style={styles.imageBackground}>
                <Text style={styles.labelWelcome}>Bem Vindo</Text>
                <Image source={require('../../assets/spotify.png')} style={styles.logoSpotify} />
                <Text style={styles.labelSpotify}>Spotify</Text>
                <Button title="Login" color="#23DE36" />
                <Text style={styles.labelOr}>or</Text>
                <Button title="Facebook" color="#388EFE" />
                <Text style={styles.labelSign}>Você ainda não tem uma conta? <Text>Crie sua conta aqui</Text></Text>
            </ImageBackground>
        </View>
    );

}

const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    imageBackground: {
        flex: 1,
        width: wp('100%'),
        height: hp('100%'),
        alignItems: 'center',
        flexDirection: 'column',
        justifyContent: 'center'
    },
    labelWelcome: {
        color: '#fff',
        fontWeight: 'bold',
        fontSize: wp('9%'),
        marginBottom: wp('8%')
    },
    labelSpotify: {
        color: '#28AF4D',
        fontWeight: 'bold',
        fontSize: wp('8%'),
        fontWeight: 'bold',
        marginTop: wp('5%'),
        marginBottom: wp('5%')
    },
    labelOr: {
        color: '#fff',
        fontSize: wp('4%'),
        marginTop: wp('2%'),
        marginBottom: wp('2%')
    },
    labelSign: {
        color: '#fff',
        fontSize: wp('4%'),
        maxWidth: wp('50%'),
        marginTop: wp('8%')
    },
    logoSpotify: {
        width: wp('18%'),
        height: hp('18%'),
        aspectRatio: 1,
    }
})
