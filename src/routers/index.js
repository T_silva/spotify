import { createAppContainer } from "react-navigation"
import { createStackNavigator } from 'react-navigation-stack'

import Welcome from '../pages/Welcome'
import Home from '../pages/Home'

const AppNavigator = createStackNavigator({

    Home: {
        screen: Home,
        navigationOptions: {
            header: null,
        }
    },
    Welcome: {
        screen: Welcome,
        navigationOptions: {
            header: null,
        }
    },


})

export default Router = createAppContainer(AppNavigator)